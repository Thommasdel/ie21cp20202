// Trabalho final IE-Arduino
/*Algoritmo Automação Residencial
O nosso projeto funciona da seguinte maneira:
Ao inicializar o dispositivo pela primeira vez, ele ficará aguar
dando a consiguração. Para inicializar a configuração, o usuário 
deverá apertar a tecla (FUNC/STOP)[pisca duas vezes]. Em seguida
deverá apertar o botão que deseja programar (1-9)[pisca duas vez
es], esse botão ficará associado ao controlador em questão. Após 
isso, o controlador aguardará um novo comando (1 ou 2), se pressi
onado 1, o Relé ficará definido em modo pulso, então toda vez que
o usuário pressionar o botãoa ssociado ao dispositivo, o relé dará
um pulso de um segundo e voltará ao estado NA; se pressionado 2, 
o relé será definido como troca de estado, onde toda vez que o 
usuário pressionar o botão relacionado ao dispositivo, o relê alte
rnará entre o estado normal aberto e normal fechado.

Funcionamento: Após configurado, o display informará a tecla ao 
qual o receptor está relacionado, e toda vez que for pressionado
o botão em questão[], o relê responderá, dependendo de qual modo 
foi configurado.*/

#include <IRremote.h>
#define LED1 10
#define IR 11
#define RLE 12
#define BTN 13

IRrecv recep(IR);
decode_results result;

bool state = false; // if true, dispositivo configurado
bool value = false; // Estado do relê(2)
int configs; // Parâmetros do receptor
int rleState; // Modo do relê
int countDisplay = 0;
int rleSenha[4]; // Senha de quatro digitos
int checkSenha[4];

void setup()
{
  // Saídas do display pinos 9 ao 2
  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(2, OUTPUT);
  
  pinMode(LED1, OUTPUT); // LED1
  pinMode(BTN, INPUT); // Botão de reset
  
  recep.enableIRIn(); // Receptor IR
  
  pinMode(RLE, OUTPUT);
  
  Serial.begin(9600);
}

void loop()
{
  int button;
  // Lê o controle
  button = call_button();
  
  if (state == false)
  {
    displayZero(state);
    offline(countDisplay);
    countDisplay++;
    
    if(countDisplay == 6)
      countDisplay = 0;
  }
    
  // Programa o controlador
  if (button == 16 && state == false)
  {
    displayZero(state);
    configs = set_device(); // número do dispositivo
    
    if (configs != 0)
      rleState = set_relay(); // Estado do relay
    
    if (rleState == 3)
    {
      for (int i = 0; i < 4; i++)
      {
        button = -1;
        while (button < 0 || button > 9)
        {
          button = call_button();
          ctrl_led(1);
          display(0);
          digitalWrite(9, LOW);
          rleSenha[i] = button;
        }
      }
      
      display(configs);
      
      digitalWrite(RLE, HIGH);
    }
    // Confere se o botão foi válido para habilitar o dispositivo
    if (configs >= 0 && configs < 10)
      state = true;
    
    digitalWrite(9, HIGH);
  }
  
  // Uso do controlador
  else if (state == true)
  {
    if (button == configs && rleState != 3)
    {
      relay_state(rleState, value);
      
      ctrl_led(1);
      
      if (rleState == 2)
      {
        value = !value;
      }
    }
    
    else if (button == configs && rleState == 3)
    {
      int j = 0;
      
      for (int i = 0; i < 4; i++)
        {
          button = -1;
          while (button < 0 || button > 10)
          {
            button = call_button();
            ctrl_led(1);
            checkSenha[i] = button;
          }
        }
      
      for (int i = 0; i < 4; i++)
      {
        if (checkSenha[i] == rleSenha[i])
          j++;
        
        Serial.println(checkSenha[i]);
        Serial.println(rleSenha[i]);
        Serial.println(j);
      }
      
      if (j == 4)
      {
      	relay_state(1, value); 
        ctrl_led(1);
      }
    }
  }
}


                    
                    
                    
                    
                    
                    
                    

int call_button(void)
{
  long int button;
  int newButton;
  
  if(recep.decode(&result))
  {
    button = result.value;
    newButton = ctrl_map(button);
    //Serial.println(result.value); // Debugger
    recep.resume();
  }
  
  return newButton;
}


// Essa função é usada para identificar o dispositivo
int set_device(void)
{
  int button = -1;

  while(button < 0 || button > 21)
  {
    ctrl_led(1);
    button = call_button();
  } 
  
  if (button > 0 && button < 10)
  {
    ctrl_led(2);
    display(button);
    return button;
  }
  
  else
  {
    if (button == 0)
      button = -1;
    
    display(button);
    ctrl_led(5);
    button = 0;
    return button;
  }
}

// Set_relay irá definir o tipo de sinal que o relê devolverá
int set_relay(void)
{
  int button = -1;
  // Garante que os botões para configuração serão dados por 1 e 2
  while(button < 1 || button > 3)
  {
    digitalWrite(9, LOW);
    ctrl_led(1);
    button = call_button();
  }
  
  if (button == 1)
  {
    digitalWrite(RLE, HIGH);
    ctrl_led(3); // Pulso
  }

  else if (button == 2)
    ctrl_led(4); // Troca de estado
  
  else
    ctrl_led(1);

  return button;
}

// Apaga o display
void displayZero(bool state)
{
  if (state == false)
  {
    for(int i = 2; i < 10; i++)
    {
      digitalWrite(i, HIGH);
    }
  }
}

// Mapeia '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' e 'E'
long int map_display(int x)
{
  	if (x == 0)
        return 11; // Imprime 0

    else if (x == 1)
        return 10011111; // Imprime 1

    else if (x == 2)
        return 100101; // Imprime 2

    else if (x == 3)
        return 1101; // Imprime 3

    else if (x == 4)
        return 10011001; // Imprime 4

    else if (x == 5)
        return 1001001; // Imprime 5

    else if (x == 6)
        return 1000001; // Imprime 6

    else if (x == 7)
        return 11111; // Imprime 7

    else if (x == 8)
        return 1; // Imprime 8

    else if (x == 9)
        return 1001; // Imprime 9
      
    else
        return 1100001; // Imprime E
}

// Movimento de standby no display
void offline(int count)
{
  long int binary;
  int num[8];
  
  if (count == 0)
    binary = 111111;
  
  if (count == 1)
    binary = 10011111;
  
  if (count == 2)
    binary = 11001111;
  
  if (count == 3)
    binary = 11100111;
  
  if (count == 4)
    binary = 11110011;
  
  if (count == 5)
    binary = 1111011;
  
  for (int i = 7; i >= 0; i--)
  {
    if (binary % 10 == 1 && binary != 0)
      num[i] = 1;
    
    else if (binary % 10 == 0 || binary == 0)
      num[i] = 0;
      
    binary /= 10;
  }
  
  for (int i = 0; i < 8; i++)
  {
    if (num[i] == 1)
      digitalWrite(i + 2, HIGH);
    
    else if (num[i] == 0)
      digitalWrite(i + 2, LOW);
  }
  
  delay(75); // 300ms IRL
}

// Essa função acende o define um valor para o display de 0 a 9
void display(int y)
{
  long int binary;
  int num[8];
  
  binary = map_display(y); // y = tecla do controle que ainda há de ser mapeado
  
  for (int i = 7; i >= 0; i--)
  {
    if (binary % 10 == 1 && binary != 0)
      num[i] = 1;
    
    else if (binary % 10 == 0 || binary == 0)
      num[i] = 0;
      
    binary /= 10;
  }
  
  for (int i = 0; i < 8; i++)
  {
    if (num[i] == 1)
      digitalWrite(i + 2, HIGH);
    
    else if (num[i] == 0)
      digitalWrite(i + 2, LOW);
  }
}




// Função com os comportamentos do relé
void relay_state(int value, int boolValue)
{
  if (value == 1) // Pulso
  {
    digitalWrite(RLE, LOW);
    delay(1000); // 4000 IRL
    digitalWrite(RLE, HIGH);
  }
  
  else if (value == 2) // Troca de estado
  {
    if(boolValue == false)
    {
      digitalWrite(RLE, HIGH);
    }
    
    else if(boolValue == true)
    {
      digitalWrite(RLE, LOW);
    }
  }
}


// Converte o código dos botões do controle em int para facilitar o uso
int ctrl_map(long int button)
{
  int i;
  
  if(button==16593103)
    i = 0;//botao0
  
  else if(button==16582903)
    i = 1;//botao 1
   
  else if(button==16615543)
    i = 2;//botao 2
   
  else if(button==16599223)
    i = 3;//botao 3
   
  else if(button==16591063)
    i = 4;//botao 4 
   
  else if(button==16623703)
    i = 5;//botao 5
   
  else if(button==16607383)
    i = 6;//botao 6
   
  else if(button==16586983)
    i = 7;//botao 7
   
  else if(button==16619623)
    i = 8;//botao 8
   
  else if(button==16603303)
    i = 9;//botao 9
   
  else if(button==16580863)
    i = 10;//botao power
   
  else if(button==16613503)
    i = 11;//botao vol +
   
  else if(button==16617583)
    i = 12;//botao vol - 
   
  else if(button==16589023)
    i = 13;//botao volta esquerda 
   
  else if(button==16605343)
    i = 14;//botao avanca direita 
   
  else if(button==16621663)
    i = 15;//botao play/pause 
   
  else if(button==16597183)
    i = 16;//botao func/stop
   
  else if(button==16584943)
    i = 17;//botao para baixo
   
  else if(button==16601263)
    i = 18;//botao para cima
   
  else if(button==16625743)
    i = 19;//botao eq
   
  else if(button==16609423)
    i = 20;//botao st/rept
    
  return i;
}


// Função utilizada para controlar os leds. Detalhamento no corpo da função
void ctrl_led(int state)
{
  int i = 0;

  // Informa troca do estado do relê
  if (state == 1) // opção 1 (total 500 ms)
  {
      digitalWrite(LED1, HIGH);
      delay(100); // 250ms IRL
      digitalWrite(LED1, LOW);
  }
  
  // Informa êxito
  else if (state == 2) // opção 2 (total 1000 ms)
  {
    // Acende e apaga 2 vezes com intervalos de 250ms
    while(i < 2)
    {
      // 250ms IRL
      digitalWrite(LED1, HIGH);
      delay(100);
      digitalWrite(LED1, LOW);
      delay(100);
      i++;
    }
  }
  
  // Utilizado no relê-pulso
  else if (state == 3) // opção 3 (total 1750 ms)
  {
    // Acende por 500 ms e apaga por 250 ms IRL
    digitalWrite(LED1, HIGH);
    delay(250);
    digitalWrite(LED1, LOW);
    delay(100);
    // Acende e apaga 2 vezes com intervalos de 250ms IRL
    while(i < 2)
    {
      digitalWrite(LED1, HIGH);
      delay(100);
      digitalWrite(LED1, LOW);
      delay(100);
      i++;
    }
  }
  
  // Utilizado para o relê-troca-de-estado 
  else if (state == 4) // opção 4 (total 1500 ms)
  {
    // Acende e apaga 3 vezes com intervalos de 250ms IRL
    while(i < 3)
    {
      digitalWrite(LED1, HIGH);
      delay(100);
      digitalWrite(LED1, LOW);
      delay(100);
      i++;
    }
  }
  
  // Utilizado nos erros
  else if (state == 5) // opção 5
    digitalWrite(LED1, HIGH); // fica aceso
}


// Uma função para resetar tudo pelo botão
/* Essa função funcionará de duas maneiras distintas
   segurando o botão ou apertando o botão de power do controle
   Porém, caso seja pelo controle, o usuário deverá digitar uma
   senha para confirmar o reset. A senha deverá ter 4 characteres
   e por npadrão será 1234, mas pode ser configurada.
*/

//O botão já funciona, agora falta fazer a função de resetar o dispositivo (falta também decidir qual será o botão para a troca de senha)
bool reset(void)
{
  
}
